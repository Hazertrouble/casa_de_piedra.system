/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventas;


/**
 *
 * @author srhaz
 */
public class histoVenta {
    
    private String fecha;
    private float importe;
    private String cliente;
    private String Tipo;
    private String emple;

    public histoVenta(String fecha, float importe, String cliente, String Tipo, String emple) {
        this.fecha = fecha;
        this.importe = importe;
        this.cliente = cliente;
        this.Tipo = Tipo;
        this.emple = emple;
    }

    public histoVenta() {
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public float getImporte() {
        return importe;
    }

    public void setImporte(float importe) {
        this.importe = importe;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public String getEmple() {
        return emple;
    }

    public void setEmple(String emple) {
        this.emple = emple;
    }
    
    
}
