/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventas;

/**
 *
 * @author srhaz
 */
public class productosVenta {
    private String producto;
    private int existencias;
    private int descuento;
    private float precio;
    private float neto;

    public productosVenta(String producto, int existencias, int descuento, float precio) {
        this.producto = producto;
        this.existencias = existencias;
        this.descuento = descuento;
        this.precio = precio;
        this.neto = valorNeto();
    }

    public productosVenta() {
    }
    
    public float valorNeto(){
        float descu = (float) (descuento * .01); 
        float x = precio * descu;
        return precio - x;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public int getExistencias() {
        return existencias;
    }

    public void setExistencias(int existencias) {
        this.existencias = existencias;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getNeto() {
        return neto;
    }
    
    
}
