/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productos;


public class producto {
    private int id;
    private String nombre;
    private String marca;
    private String tipo;
    private int cantidad;
    private String unidad;
    private int existencias;
    private float precioVenta;
    private float precioCompra;
    private int promo;

    public producto(int id, String nombre, String marca, String tipo, int cantidad, String unidad, int existencias, float precioVenta, float precioCompra, int promo) {
        this.id = id;
        this.nombre = nombre;
        this.marca = marca;
        this.tipo = tipo;
        this.cantidad = cantidad;
        this.unidad = unidad;
        this.existencias = existencias;
        this.precioVenta = precioVenta;
        this.precioCompra = precioCompra;
        this.promo = promo;
    }
    
    public producto(int id, String nombre, String marca, String tipo, int cantidad, String unidad, int existencias, float precioVenta, float precioCompra) {
        this.id = id;
        this.nombre = nombre;
        this.marca = marca;
        this.tipo = tipo;
        this.cantidad = cantidad;
        this.unidad = unidad;
        this.existencias = existencias;
        this.precioVenta = precioVenta;
        this.precioCompra = precioCompra;
    }

    public producto() {
    }

    public int getId() {
        return id;
    }

    public int getPromo() {
        return promo;
    }

    public void setPromo(int promo) {
        this.promo = promo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public int getExistencias() {
        return existencias;
    }

    public void setExistencias(int existencias) {
        this.existencias = existencias;
    }

    public float getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(float precioVenta) {
        this.precioVenta = precioVenta;
    }

    public float getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(float precioCompra) {
        this.precioCompra = precioCompra;
    }
    
    
    
}
