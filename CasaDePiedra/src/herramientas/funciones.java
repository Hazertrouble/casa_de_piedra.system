/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herramientas;

import ventas.Facturas_clie;
import ventas.Histo_ventas;
import casadepiedra.*;
import clientes.*;
import empleados.*;
import productos.*;
import proveedores.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JFrame;
import promociones.Alta_promo;
import promociones.Baja_promo;
import promociones.Cambio_promo;
import promociones.Lista_promo;
import promociones.Relacionar_promo;
import promociones.promocion;
import ventas.histoVenta;
import ventas.productosVenta;

public class funciones {
   MySqlConn conn;
   
    public funciones() {
        conn = new MySqlConn();
    }
    
    // Cambia a otro JFrame
    public void irA (String pantalla, String a, int x, int y){
        JFrame P;
        switch(pantalla){
            case "login": P = new Login(); break;
            case "admin": P = new MainMenu_Admin(a); break;
            case "emp": P = new MainMenu_Employee(a); break;
            case "L_emp": P = new Lista_emp(); break;
            case "L_cli": P = new Lista_cliente(); break;
            case "L_prod": P = new Lista_prod(); break;
            case "L_prove": P = new Lista_prove(); break;
            case "A_emp": P = new Alta_emp(); break;
            case "A_cli": P = new Alta_cliente(); break;
            case "A_prod": P = new Alta_prod(); break;
            case "A_prove": P = new Alta_prove(); break;
            case "B_emp": P = new Baja_emp(); break;
            case "B_cli": P = new Baja_cliente(); break;
            case "B_prod": P = new Baja_prod(); break;
            case "B_prove": P = new Baja_prove(); break;
            case "C_emp": P = new Cambio_emp(a); break;
            case "C_cli": P = new Cambio_cliente(a); break;
            case "C_prod": P = new Cambio_prod(a); break;
            case "C_prove": P = new Cambio_prove(a); break;
            case "H_ventas": P = new Histo_ventas(); break;
            case "Fac_cli": P = new Facturas_clie(); break;
            case "Fac_prove": P = new Facturas_prove(); break;
            case "Visit_prove": P = new Visita_prove(); break;
            case "A_promo": P = new Alta_promo(); break;
            case "B_promo": P = new Baja_promo(); break;
            case "C_promo": P = new Cambio_promo(a); break;
            case "R_promo": P = new Relacionar_promo(a); break;
            case "L_promo": P = new Lista_promo(); break;
            
            default: P = null;
        }
        
        System.out.println("");
        try{
            P.setLocation(x, y);
            P.setVisible(true);
            P.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            System.out.println("  -- Ahora en ' "+pantalla+" '");
        }catch(Exception e){
            
            System.out.println("  - Error al ir a ' "+pantalla+" '");
        }
    }
    
    // Consulta una query
    public void consultarQuery(String query){
        System.out.println("CONSULTANDO : ' "+query+" '");
        conn.Update(query);
        conn.closeRsStmt();
        conn.closeConnection();
    }
    
    // Retorn T or F si el texto ingresado es un RFC valido. alphanumerico de longitud 13 exacta.
    public boolean isRFCvalid(String str){
        boolean alpha=false, numeric=false, largo=false;
        
        for (char c : str.toCharArray()){
            if (Character.isDigit(c))numeric=true;
            if (Character.isLetter(c))alpha=true;
        }
        
        if(str.length()== 13)largo=true;
        
        // Si no tiene caracteres o numero o no es 13 de largo, no es un rfc valido
        if(alpha==false || numeric==false || largo==false){
            System.out.println("No es un RFC valido");
            return false;
        }
        
        System.out.println("Si es un RFC valido");
        return true;
    }
    
    // Retorna el T or F si el texto ingresado es un Numero sin caracteres
    public boolean isNumeric(String str){
        if (str == null) {
            System.out.println("Es un valor nulo");
            return false;
        }
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            System.out.println("No es un Numero"); 
            return false;
        }
        System.out.println("Si es un numero");
        return true;
    }
    
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //     FUNCIONES DE LISTAS
    
    
    //retorna una lista de empleados resultado de una consulta-query
    public ArrayList<empleado> obtenerListaEmpleado(String query) throws SQLException{
        ArrayList<empleado> y = new ArrayList<>();
        empleado a = new empleado();
        
        conn.Consult(query);
        do{
            a.setIdEmpleado(conn.rs.getInt(1));
            a.setNombre(conn.rs.getString(2));
            a.setApePaterno(conn.rs.getString(3));
            a.setApeMaterno(conn.rs.getString(4));
            a.setTelefono(Long.valueOf(conn.rs.getString(5)));
            a.setDomicilio(conn.rs.getString(6));
            a.setPuesto(conn.rs.getString(7));
            a.setPassword(conn.rs.getString(8));
            
            y.add(new empleado(a.getIdEmpleado(),a.getNombre(),a.getApePaterno(),a.getApeMaterno(),a.getTelefono(),a.getDomicilio(),a.getPuesto(),a.getPassword()));
            System.out.print(y.size());
        }while(conn.rs.next());
        
        System.out.println(" - Se lleno lista de empleados");
        return y;
    }
    
    //retorna una lista de clientes resultado de una consulta-query
    public ArrayList<cliente> obtenerListaCliente(String query) throws SQLException{
        ArrayList<cliente> c = new ArrayList<>();
        cliente a = new cliente();
        
        conn.Consult(query);
        do{
            a.setRFC(conn.rs.getString(1));
            a.setNombre(conn.rs.getString(2));
            a.setApePaterno(conn.rs.getString(3));
            a.setApeMaterno(conn.rs.getString(4));
            a.setTelefono(Long.valueOf(conn.rs.getString(5)));
            a.setDomicilio(conn.rs.getString(6));
         
            c.add(new cliente(a.getRFC(),a.getNombre(),a.getApePaterno(),a.getApeMaterno(),a.getTelefono(),a.getDomicilio()));
            System.out.print(c.size());        
        }while(conn.rs.next());
        
        System.out.println(" - Se lleno lista de clientes");
        return c;
    }
    
    //retorna una lista de proveedores resultado de una consulta-query
    public ArrayList<proveedor> obtenerListaProveedor(String query) throws SQLException{
        ArrayList<proveedor> x = new ArrayList<>();
        proveedor a = new proveedor();
        
        conn.Consult(query);
        do{
            a.setId(conn.rs.getInt(1));
            a.setNombre(conn.rs.getString(2));
            a.setEmpresa(conn.rs.getString(3));
            a.setApePaterno(conn.rs.getString(4));
            a.setApeMaterno(conn.rs.getString(5));
            a.setTelefono(Long.valueOf(conn.rs.getString(6)));
         
            x.add(new proveedor(a.getId(),a.getNombre(),a.getEmpresa(),a.getApePaterno(),a.getApeMaterno(),a.getTelefono()));
            System.out.print(x.size());
        }while(conn.rs.next());
        
        System.out.println(" - Se lleno lista de proveedores");
        return x;
    }
    
    //retorna una lista de proveedores resultado de una consulta-query
    public ArrayList<producto> obtenerListaProductos(String query, int opc) throws SQLException{
        ArrayList<producto> x = new ArrayList<>();
        producto a = new producto();
        
        conn.Consult(query);
        do{
            a.setId(conn.rs.getInt(1));
            a.setNombre(conn.rs.getString(2));
            a.setMarca(conn.rs.getString(3));
            a.setTipo(conn.rs.getString(4));
            a.setCantidad(conn.rs.getInt(5));
            a.setUnidad(conn.rs.getString(6));
            a.setExistencias(conn.rs.getInt(7));
            a.setPrecioVenta(conn.rs.getFloat(8));
            a.setPrecioCompra(conn.rs.getFloat(9));
            if(opc==1){
                a.setPromo(conn.rs.getInt(10));
                x.add(new producto(a.getId(),a.getNombre(),a.getMarca(),a.getTipo(),a.getCantidad(),a.getUnidad(),a.getExistencias(),a.getPrecioVenta(),a.getPrecioCompra(),a.getPromo()));
            }
            else x.add(new producto(a.getId(),a.getNombre(),a.getMarca(),a.getTipo(),a.getCantidad(),a.getUnidad(),a.getExistencias(),a.getPrecioVenta(),a.getPrecioCompra()));
                     
            System.out.print(x.size());
        }while(conn.rs.next());
        
        System.out.println(" - Se lleno lista de productos");
        return x;
    }
       
    //retorna una lista de ventas resultado de una consulta-query
    public ArrayList<histoVenta> obtenerListaHistorial(String query) throws SQLException{
        ArrayList<histoVenta> x = new ArrayList<>();
        histoVenta a = new histoVenta();
        
        conn.Consult(query);
        do{
            a.setFecha(conn.rs.getString(1));
            a.setImporte(conn.rs.getFloat(2));
            a.setCliente(conn.rs.getString(3));
            a.setTipo(conn.rs.getString(4));
            a.setEmple(conn.rs.getString(5));
            
            x.add(new histoVenta(a.getFecha(),a.getImporte(),a.getCliente(),a.getTipo(),a.getEmple()));
                     
            System.out.print(x.size());
        }while(conn.rs.next());
        
        System.out.println(" - Se lleno lista de ventas");
        return x;
    }
    
    //retorna una lista de promociones resultado de una consulta-query
    public ArrayList<promocion> obtenerListaPromo(String query) throws SQLException{
        ArrayList<promocion> x = new ArrayList<>();
        promocion a = new promocion();
        
        conn.Consult(query);
        do{
            a.setCodigo(conn.rs.getInt(1));
            a.setNombre(conn.rs.getString(2));
            a.setDescripcion(conn.rs.getString(3));
            a.setDescuento(conn.rs.getInt(4));
            a.setFecha(conn.rs.getString(5));
            
            x.add(new promocion(a.getCodigo(),a.getNombre(),a.getDescripcion(),a.getDescuento(),a.getFecha()));
                     
            System.out.print(x.size());
        }while(conn.rs.next());
        
        System.out.println(" - Se lleno lista de ventas");
        return x;
    }
    
    //retorna una lista de promociones resultado de una consulta-query
    public ArrayList<productosVenta> obtenerListaProductosVenta(String query) throws SQLException{
        ArrayList<productosVenta> x = new ArrayList<>();
        productosVenta a = new productosVenta();
        
        conn.Consult(query);
        do{
            a.setProducto(conn.rs.getString(1));
            a.setExistencias(conn.rs.getInt(2));
            a.setDescuento(conn.rs.getInt(3));
            a.setPrecio(conn.rs.getFloat(4));
            
            x.add(new productosVenta(a.getProducto(),a.getExistencias(),a.getDescuento(),a.getPrecio()));
                     
            System.out.print(x.size());
        }while(conn.rs.next());
        
        System.out.println(" - Se lleno lista de los productos en Venta");
        return x;
    }
}
